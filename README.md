# Contour Matrix

Relating basic informational concepts in a three dimensional matrix.

The Latex code for the corresponding document published at
<https://dedizier.de/customlabs>

---

## License

Long ago the Latex parts were forked from the following.  Thanks and license
prevail and extend to the entire document.

[LaTeX-Vorlage zur Projektdokumentation für Fachinformatiker
Anwendungsentwicklung](http://fiae.link/LaTeXVorlageFIAE
"Vorlage für die Projektdokumentation")

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img
alt="Creative Commons License" style="border-width:0"
src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work
is licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons
Attribution-ShareAlike 4.0 International License</a>.
