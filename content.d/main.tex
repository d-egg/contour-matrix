\section{Contour Matrix}
\label{sec:a_primer}

\subsection{Setup}
\label{sec:setup}

Without too much ado, I'd like to give you an update of where I am with this
project.  It is a little unpolished simply due to how things evolve.  Of course
I'd like to present you with something final, but maybe this brevity allows for
your own interpretation and novel views regarding these concepts.  Try to take
the terms I use rather loosely and fathom the underlying features.

A short additional note on threes.  I think if there is a balance without one
leg of another perspective into that balance, it will become stagnant.  You need
an additional domain to make energy continue to flow through cells, navigate to
another situation, or predict a trajectory relative to the surroundings.

For a long time I have been looking for complements like diverse/similar or
many/one, but those dualities did not really fit together.  When I started to
search for \TechnicalTerm{triametrically opposed (TO)} concepts instead, things
worked out a lot better ("tria" is probably short for \textit{TReIs-fetA}).

\subsection{Workshop Template}
\label{sec:workshop-template}

\begin{figure}[!h]
\centering
\includegraphicsKeepAspectRatio{
    reduced/WS-template--threes--center-cube-03.png}{1}
\caption{Workshop Template}
\label{fig:workshop-template-overview}
\end{figure}

In \autoref{fig:workshop-template-overview} is a rearranged implementation from
the contour-starter document that includes nesting (inner/outer), together with
parallel (rotation), and sequential (dim-depth) structures.  You can see that
there are six pistons of which there are two per room dimension of UDB tendency,
like before.  I refer to them as lats, from the word lateral, as they can
represent the extremes of a particular perspective at the center.

The roles have been merged into a specific setup.  There are variations of this
arrangement possible which relate to how the context is made up.  Imagine
a permeating project fabric of blue spikes, collaborating trefoil teams, or
a consolidated client scaffolding of cubes external to that picture.

The nesting containers in the Buzzing dimension are based on kites now.  The
kite might be a stable inner corner instead of the stable outer corner of
a rectangle.  I'm using prolonged 2D shapes to emphasize the facades on each
end.  The 3D context might consist of spheres, cuboids, and octahedra (but
defined by the kite).

The shapes on the far side of the pistons stand for continuous Concrete (green),
categorization Fictitious (yellow), and intensity Deviation (blue) ethers.

I will refer to this template when explaining the cube-matrix of concepts at
the center of the picture.

\subsection{Cube Matrix}
\label{sec:cube-matrix}

There are three fundamental categories of Matter (MSW), Mental (CST), and Energy
(UDB).  Those are the three perpendicular dimensions in the cube-matrix in
\autoref{fig:cube-matrix-3D} .  You can see the matrix in the 2D
\autoref{tab:cube-matrix}, but have to imagine the UDB dimension for the
triplets.

There are three variants in each of these categories.  \textbf{Matter} consists
of Motion, Stop, Wiring.  \textbf{Mental} consists of Context, Self, Tools.
\textbf{Energy} consists of Union, Disjoint, Buzzing.

The UDB triplets are enumerated in \autoref{tab:cube-matrix-acronyms}.

\begin{tableEnv}
    \input{tables/cube-matrix}
    \caption{Cube Matrix with UDB triplets}
    \label{tab:cube-matrix}
\end{tableEnv}

\begin{figure}[!h]
\centering
\includegraphicsKeepAspectRatio{reduced/Cube-matrix-03.png}{0.5}
\caption{Cube Matrix with three main categories}
\label{fig:cube-matrix-3D}
\end{figure}

\begin{tableEnv}
    \input{tables/cube-matrix-categories}
    \caption{Cube Matrix Categories}
    \label{tab:cube-matrix-categories}
\end{tableEnv}

Some of the elements in the matrix are in a different order compared to the
contour-starter document.  I failed to order some concepts according to UDB
before, which is the origin of most of these concepts.  Disentangling all the
notions that were associated to UDB resulted in this concept map, which you
might consider to be part of the categorization Fictitious ether.

I have further categorized the triplets in UDB groups, as can be seen in
\autoref{tab:cube-matrix-categories}.  Other groupings are certainly possible,
and the piston alignments might indicate a functional interaction of those
concepts.  As an introduction, I hope these groupings are helpful to think about
these concepts.

\begin{tableEnv}
    \input{tables/cube-matrix-acronyms}
    \caption{Cube Matrix Acronyms}
    \label{tab:cube-matrix-acronyms}
\end{tableEnv}

You know the \textbf{Dynamics} and \textbf{Ethers} from the previous document.

\textbf{Communication} in this Context row of MSW columns are variations on how
you interact with others in UDB variants.  I took the words Collaborate,
Cooperate, Coordinate and associated them with UDB properties.  Collaborate in
small manageable scopes where you search for solutions to a particular problem.
The interferences of a Union interaction dominate in this perspective.
Cooperation is the assignment of responsibilities in a disjoint scaffolding to
solve more comprehensive problems.  Coordinate progress of such stages in a
development pipeline, which has a linear branch/merge notion of progress.

In the Tool row is the \textbf{Potential} category with Topology, Situation,
Flux elements.  Generally this is a sort of multiplicity present in automations.
It might be considered an accretion of the corresponding UDB feature which can
constrict to a specific outcome.  A union Topology might have a tendency to
transform in certain ways, a Situation might become meaningful depending on the
experiences of a particular observer, and a riverbed might lead to specific
eddies.

\textbf{Organize} data into Parallel, Nesting, Sequential structures.  The
pistons mentioned above are a helpful visual representation of these structures.

In the \textbf{Geometry} category are Phase, Ratio, Angle. These would be the
metrics of a substance and accordingly might suggest some purpose to an actor.
This category term refers to different elements in contrast to the previous
document.

\textbf{Profusion} is the category for the triplet of Compression, Diffusion,
Progression. It refers to how to enact change by colocating
a \TechnicalTerm{Workpiece (WP)} with \TechnicalTerm{Working Knowledge (WK)}.
Think of gradually computing intermediate cases, fading out details to treat
them as coarsely similar, and a migration adjustment.  I assume that the
non-linear volume expansion is TO to specification and progression features.

The \textbf{Difference} category consists of Pivot, Brachiate,
Counter-direction.  The idea is that you can understand difference as something
orthogonally adjusted around center points, a clustering/matching of nested
elements, or a counter directional flow displacement.  In each case, there is
a fixed invariant and a degree of freedom.

The \textbf{Instantiation} has a notion of triggering communication or applying
a tool, and accordingly is about three variations of facades (API°).  You might
adjust values gradually while exploring the resulting Hues, trigger a Threshold
excitation to release a statement, or defer steps to different stages before or
after the present scope.

\subsection{Blinders as tools for perpendicular operations}
\label{sec:blinders}

In each of Energy, Matter, Mental (EMM) is one main strut that pertains to the
corresponding fundamental category.  Union in Energy, Stop in Matter, Self in
Mental.  Generally, there would be such associated positions in each of the
blinders. UDB, CTS, MSW, in that order.

This particular setup might represent a balance that is disturbed through
an external continuation of these concepts.   Like mentioned above, there might
be team, client, and project areas of such functional categorizations.

Also, the blinders for the complementary lats might be in an extremely volatile
setup and represent the feedback from areas beyond that focus.  For example, the
Generate, Concrete, Collaborate complementary green strut might represent an
active contributor.

The categories in \autoref{tab:cube-matrix-categories} are a simplification of
the combinatorial explosion from EMM, over blinders, to the cube-matrix
elements.  Note how they fade-out details of energy (UDB).  I assume that the
matter prevails over mental features in columns, while matter gets composed to
represent a mental category in rows.  But that is just an initial assumption.

\subsection{Creating Funnels: Input, Balance, Output}
\label{sec:creating-funnels}

If you look at the pistons, you might get a notion of how funnels (PWR) can be
created from those structures.  Just this time, they would refer to concepts in
the cube matrix instead of a particular program mechanism.

I have the sense, that one kind of these Par-Nest-Seq elements in a particular
constellation is superordinate, and the subordinate ones relate to another POV.

\begin{figure}[!ht]
    \begin{subfigure}{.5\linewidth}
        \centering
        \includegraphicsKeepAspectRatio{
            reduced/IBO-processing--union-02.png}{1}
        \caption{IBO in Union mode}
        \label{fig:IBO-funnel-union}
    \end{subfigure}
    \begin{subfigure}{.5\linewidth}
        \centering
        \includegraphicsKeepAspectRatio{
            reduced/IBO-processing-disjoint-02.png}{1}
        \caption{IBO in Disjoint mode}
        \label{fig:IBO-funnel-disjoint}
    \end{subfigure}
    \caption[IBO Processing]{Collapsed WS Template in different IBO modes}
    \label{fig:IBO-funnels}
\end{figure}

When replacing the cube-matrix with an actual mechanism, the workshop template
might fractionate into specific constellations.  As an example of that, consider
a particular perspective with a Union center object like in
\autoref{fig:IBO-funnel-union}.  It differentiates its breadth input funnels
from the Disjoint resource, and via the Balance funnel as a runtime-context,
constructs a Union output.  The balance funnel adjusts the two permeating POV°
as a container recursion in the center object.

In the Disjoint mode in \autoref{fig:IBO-funnel-disjoint}, a Concrete ether
evaluation feedback gets differentiated into the Balance funnel to create
a Disjoint categorization output which might have reclustering/refocusing
effects for the Fictitious ether resource used during the previous Union mode.

The back and forth would gradually approach a solution while incorporating and
implementing resources from two different Ethers.  This would be a more specific
illustration of processing compared to the ballooning notion in the previous
document.

An interesting feature of the Balance funnel in this setup is that it might
drive repetitive differentiation inwards in one dimension and conflation
outwards in another dimension.

While there is a resource abundance on input, there is merely an outline on
output to be filled.  I refer to these states of data as Stacking, Filling,
Outline.

In a similar way, the workshop templates can be in an alternating context mode
as in \autoref{fig:ws-template-opposition}.

\begin{figure}[!ht]
    \begin{subfigure}{.5\linewidth}
        \centering
        \includegraphicsKeepAspectRatio{
            reduced/Context-and-CO--trefoil-context-01.png}{1}
        \caption{WS Template Generate}
    \end{subfigure}
    \begin{subfigure}{.5\linewidth}
        \centering
        \includegraphicsKeepAspectRatio{
            reduced/Context-and-CO--cube-context-01.png}{1}
        \caption{WS Template Integrate}
    \end{subfigure}
    \caption[Workshop Template Threes]{
        Workshop Templates with different Role distribution}
    \label{fig:ws-template-opposition}
\end{figure}

\subsection{A Map of Threes}
\label{sec:map-of-threes}

The main purpose of this document is to give you an idea of how triametrically
opposed concepts can make combining different categories easier.

You can find the full size pictures at \citep{Wallenstein:full-size-pics}.
