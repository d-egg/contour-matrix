.PHONY: contour-matrix read clean clean_all

# =====

LATEXMK_SHARED_CMD = latexmk \
        -interaction=nonstopmode \
        -pdf \
        -file-line-error

# -g  Force latexmk to process document fully, even under situations where
#     latexmk would normally decide that no changes in the source files have
#     occurred since the previous run. This option is use- ful, for example, if
#     you change some options and wish to reprocess the files.
# -f        - force continued processing past errors
# -quiet    - silence progress messages from called programs
# -pdf         tells latexmk to generate PDF directly (instead of DVI).
# -pdflatex="" tells latexmk to call a specific backend with specific options.
# -use-make    tells latexmk to call make for generating missing files.

# -interaction=nonstopmode keeps the pdflatex backend from stopping at a missing
#               file reference and interactively asking you for an alternative.
# -jobname     Name of the output files (has to appear twice atm, there might be
#              a way to fix that by redefining -pdflatex or so.  Anyway...
# -latexoption Not in the docs yet but was introduced with -xelatex and
#             -lualatex switches.  Probably applies to all engines.
#

# =====

SHORTVERSION = "--jobname=$@ '$\
\newcommand{\buildtagShortVersion}{false}$\
\input{%S}$\
'"
contour-matrix:
	${LATEXMK_SHARED_CMD} \
		-xelatex \
		-jobname=$@ \
		-latexoption=${SHORTVERSION} \
		contour-matrix.tex

# ==============================
# ==============================

clean:
	latexmk -C
	-rm *.fls

clean_all:
	git clean -fx --exclude "Makefile.local"

# ==========

PDFREADER = xdg-open
bib:
	bibtex ${.DEFAULT_GOAL}
read:
	${PDFREADER} ${.DEFAULT_GOAL}.pdf
fontgrep:
	strings ${.DEFAULT_GOAL}.pdf | grep -i font
pdffonts:
	pdffonts ${.DEFAULT_GOAL}.pdf

# =====
# =====

.DEFAULT_GOAL = contour-matrix
-include Makefile.local
